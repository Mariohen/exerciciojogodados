package br.com.itau;

public class Main {

    public static void main(String[] args) {
        int somaQuantidadeDados = 0;

        Mensagens.mensagemInicial();
        Jogador jogador = new Jogador();
        jogador.quantidadeDadosJogados();
        jogador.quantidadeVezes();

       for(int v = 0; v < jogador.quantidadeVezes; v++){

            int [] lista = new int[jogador.quantidadeDados];
            for (int d = 0; d < jogador.quantidadeDados; d++){
                lista[d] = Sorteador.numeroRandomico();
                System.out.print(lista[d]);
                System.out.print(" , " );
                somaQuantidadeDados = somaQuantidadeDados + lista[d];
            }
                Mensagens.mensagemSomatorio(somaQuantidadeDados);
        }

    }
}
