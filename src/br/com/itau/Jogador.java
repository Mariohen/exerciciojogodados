package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Jogador {

    int quantidadeDados;
    int quantidadeVezes;

    public int quantidadeDadosJogados() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Quantidade de dados a serem jogados: ");
        quantidadeDados = scanner.nextInt();
        return quantidadeDados;
    }

    public int quantidadeVezes() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Quantidade de vezes a ser jogada: ");
        quantidadeVezes = scanner.nextInt();
        return quantidadeVezes;
    }
}
