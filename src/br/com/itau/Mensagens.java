package br.com.itau;

public class Mensagens {

    public static void mensagemInicial() {
        System.out.println("Bem vindo ao Jogo");
    }

    public static void mensagemSomatorio(int v) {
        System.out.print(v);
        System.out.println(" ");
    }

}
